package hu.pe.safe3city.safe3city.menu.helpers;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

// TODO: this should work asynchronously
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    private ArrayList<String> resultList;
    private final Boolean showHints = true;
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_DETAILS = "/details";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private static final String FILTER = "Gdańsk";
    private static final String API_KEY = "AIzaSyDd9FEw-E5ON8rVXvt2jzWp70NdGWJgqhY";
    private HashMap<String, String> idAndDesc;

    public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                HashMap<String, String> results;

                if (constraint != null && showHints) {
                    int len = constraint.toString().length();
                    if (len > 2) {
                        results = autocomplete(constraint.toString());
                        resultList = new ArrayList<>(results.values());
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    private HashMap<String, String> autocomplete(String input) {

        HashMap<String, String> results = new HashMap<>();
        StringBuilder jsonResults = getJsonResults(input);

        try {
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");
            for (int i = 0; i < predsJsonArray.length(); i++) {
                String filteredDescription = predsJsonArray.getJSONObject(i).getString("description");
                if (filteredDescription.contains(FILTER)) {
                    String placeId = predsJsonArray.getJSONObject(i).getString("place_id");
                    results.put(placeId, filteredDescription);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        idAndDesc = results;
        return results;
    }

    private StringBuilder getJsonResults(String input) {
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        InputStreamReader in = null;

        try {
            URL url = getUrl(input);
            conn = (HttpURLConnection) url.openConnection();
            in = new InputStreamReader(conn.getInputStream(), "UTF-8");

            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return jsonResults;
    }

    private URL getUrl(String input) {
        StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
        URL url = null;
        try {
            sb.append("?input=").append(URLEncoder.encode(input, "utf8"));
            sb.append("&types=geocode").append("&location=54.3774983,18.6142447&radius=3000");
            sb.append("&key=" + API_KEY);
            url = new URL(sb.toString());
        } catch (UnsupportedEncodingException | MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public HashMap<String, String> getIdAndDesc() {
        return idAndDesc;
    }
}

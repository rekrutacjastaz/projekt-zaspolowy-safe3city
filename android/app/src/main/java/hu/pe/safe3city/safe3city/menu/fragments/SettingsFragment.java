package hu.pe.safe3city.safe3city.menu.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import hu.pe.safe3city.safe3city.R;


public class SettingsFragment extends Fragment implements View.OnClickListener {

    private static final String SMS_FILE_NAME = "smsFIle";
    private static final String SMS_TEXT = "smsText";
    private static final String DEFAULT_REMEMBER_VALUE = "no";
    private final String DEFAULT_SMS_TEXT = "I\'m in real danger! Please help me. I\'m at %location%";
    private static final String REMEMBER_LOGIN = "rememberLogin";
    private EditText sms;
    private RadioGroup rememberRadio;
    private Integer checked = R.id.radioNo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.settings_fragment, container, false);
        Button save = (Button) rootView.findViewById(R.id.saveButton);
        save.setOnClickListener(this);
        sms = (EditText) rootView.findViewById(R.id.smsBodyEdit);
        rememberRadio = (RadioGroup) rootView.findViewById(R.id.radioGroupRemember);
        rememberRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checked = checkedId;
            }
        });
        showDataOnStart();
        return rootView;
    }

    private void showDataOnStart() {
        if (!isInPreferences(getActivity(), SMS_TEXT)) {
            saveToPreferences(getActivity(), SMS_TEXT, DEFAULT_SMS_TEXT);
            sms.setText(DEFAULT_SMS_TEXT);
        } else {
           sms.setText(readFromPreferences(getActivity(), SMS_TEXT, DEFAULT_SMS_TEXT));
        }


        if (!isInPreferences(getActivity(), REMEMBER_LOGIN)) {
            saveToPreferences(getActivity(), REMEMBER_LOGIN, DEFAULT_REMEMBER_VALUE);
        } else {
            String idString = readFromPreferences(getActivity(), REMEMBER_LOGIN, DEFAULT_REMEMBER_VALUE);
            switch (idString) {
                case "yes":
                    rememberRadio.check(R.id.radioYes);
                    break;
                case DEFAULT_REMEMBER_VALUE:
                    rememberRadio.check(R.id.radioNo);
                    break;
                default:
                    break;
            }
        }
    }

    public static Boolean isInPreferences(Context context, String preferenceName) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SMS_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.contains(preferenceName);
    }

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SMS_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue).apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SMS_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveButton:  {
                String rememberValue;
                if (checked == R.id.radioYes) {
                    rememberValue = "yes";
                } else {
                    rememberValue = DEFAULT_REMEMBER_VALUE;
                }
                saveToPreferences(getActivity(), SMS_TEXT, sms.getText().toString());
                saveToPreferences(getActivity(), REMEMBER_LOGIN, rememberValue);
                Toast.makeText(getActivity(), getString(R.string.saved_successfully), Toast.LENGTH_LONG).show();
            }
                break;
            default:
                break;
        }

    }
}

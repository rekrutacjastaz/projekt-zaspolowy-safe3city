package hu.pe.safe3city.safe3city;

public class Incident {

    private final Integer id;
    private final Integer userID;
    private final Address address;
    private final String date;
    private final String time;
    private final String victimSex;
    private final Integer victimAge;
    private final Integer incidentType;

    public Incident(Integer id, Integer userID, Address address, String date, String time, String victimSex, Integer victimAge, Integer incidentType) {
        this.id = id;
        this.userID = userID;
        this.address = address;
        this.date = date;
        this.time = time;
        this.victimSex = victimSex;
        this.victimAge = victimAge;
        this.incidentType = incidentType;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserID() {
        return userID;
    }

    public Address getAddress() {
        return address;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getVictimSex() {
        return victimSex;
    }

    public Integer getVictimAge() {
        return victimAge;
    }

    public Integer getIncidentType() {
        return incidentType;
    }


}

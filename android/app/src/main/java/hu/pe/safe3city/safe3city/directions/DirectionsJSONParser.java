package hu.pe.safe3city.safe3city.directions;


import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DirectionsJSONParser {

    private static final String STATUS_OK = "OK";

    public List<List<HashMap<String,String>>> parse(JSONObject jObject) {

        List<List<HashMap<String, String>>> routes = new ArrayList<>() ;
        GooglePolylineDecoder polylineDecoder = new GooglePolylineDecoder();

        JSONArray jRoutes;
        JSONArray jLegs;
        JSONArray jSteps;
        JSONObject jDistance;
        JSONObject jDuration;

        try {
            if (STATUS_OK.equals(jObject.getString("status"))) {
                jRoutes = jObject.getJSONArray("routes");

                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ((JSONObject) jRoutes.get(i)).getJSONArray("legs");
                    List<HashMap<String, String>> path = new ArrayList<>();

                    for (int j = 0; j < jLegs.length(); j++) {

                        jDistance = ((JSONObject) jLegs.get(j)).getJSONObject("distance");
                        HashMap<String, String> distanceHashMap = new HashMap<>();
                        distanceHashMap.put("distance", jDistance.getString("text"));

                        jDuration = ((JSONObject) jLegs.get(j)).getJSONObject("duration");
                        HashMap<String, String> durationHashMap = new HashMap<>();
                        durationHashMap.put("duration", jDuration.getString("value"));

                        path.add(distanceHashMap);
                        path.add(durationHashMap);

                        jSteps = ((JSONObject) jLegs.get(j)).getJSONArray("steps");

                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline;
                            polyline = (String) ((JSONObject) ((JSONObject) jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = polylineDecoder.decodePoly(polyline);
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<>();
                                hm.put("lat", Double.toString((list.get(l)).latitude));
                                hm.put("lng", Double.toString((list.get(l)).longitude));
                                path.add(hm);
                            }
                        }
                    }
                    routes.add(path);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return routes;
    }
}
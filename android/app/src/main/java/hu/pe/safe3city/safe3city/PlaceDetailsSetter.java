package hu.pe.safe3city.safe3city;

import com.google.android.gms.maps.model.LatLng;

public interface PlaceDetailsSetter {

    void setStartLocation(LatLng startLocation);

    void setEndLocation(LatLng endLocation);
}

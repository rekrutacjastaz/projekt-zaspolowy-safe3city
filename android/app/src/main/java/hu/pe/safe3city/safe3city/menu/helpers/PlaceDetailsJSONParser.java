package hu.pe.safe3city.safe3city.menu.helpers;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

public class PlaceDetailsJSONParser {

    public LatLng parse(JSONObject jObject) {

        LatLng placeLocation;
        String lat = null;
        String lng = null;

        try {
            //TODO: check status and raise error if it's not "OK"
            JSONObject jResult = jObject.getJSONObject("result");
            JSONObject jGeometry = jResult.getJSONObject("geometry");
            JSONObject jLocation = jGeometry.getJSONObject("location");
            lat = jLocation.getString("lat");
            lng = jLocation.getString("lng");
        } catch (Exception e) {
            e.printStackTrace();
        }
        placeLocation = new LatLng(Double.valueOf(lat), Double.valueOf(lng));
        return placeLocation;
    }


}

package hu.pe.safe3city.safe3city;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import hu.pe.safe3city.safe3city.directions.DirectionsRouteDrawer;

public class RoutingActivity extends ActionBarActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener {

    private static final String START = "start";
    private static final String END = "end";
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private GoogleMap map;
    private MapView mapView;
    private LatLng startLocation;
    private LatLng endLocation;
    private Integer mapType;
    private String routeMode;
    private CheckConnection checkConnection;
    private List<LatLng> route;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Boolean started = false;
    private ArrayList<Incident> allIncidents;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDataFromIntent();
        setContentView(R.layout.activity_routing);
        toolbarInit();
        mapView = (MapView) findViewById(R.id.mapRouting);
        mapView.onCreate(savedInstanceState);
        checkConnection = new CheckConnection(this);
        mapView.getMapAsync(this);

        Button startRouting = (Button) findViewById(R.id.startRouting);
        startRouting.setOnClickListener(this);
        Button stopRouting = (Button) findViewById(R.id.stopRouting);
        stopRouting.setOnClickListener(this);

        try {
            MapsInitializer.initialize(getApplication());
        } catch (Exception e) {
            e.printStackTrace();
        }
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10000)
                .setFastestInterval(1000);
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            alertWhenNoGPS();
        }
    }

    private void alertWhenNoGPS() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.turn_on_gps))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getString(R.string.cancel_button), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        closeMap();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();

    }

    private void closeMap() {
        this.finish();
    }

    private void getDataFromIntent() {
        Bundle data = getIntent().getExtras();
        startLocation = (LatLng) data.get("startLocation");
        endLocation = (LatLng) data.get("endLocation");
        mapType = data.getInt("mapType", GoogleMap.MAP_TYPE_NORMAL);
        routeMode = data.getString("routeMode", "walking");
    }

    private void toolbarInit() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void setUpMap() {
        map.setMapType(mapType);
        getIncidentsFromDB();
    }


    private void drawRoute(LatLng start, LatLng end, Boolean draggable) {
        DirectionsRouteDrawer routeDrawer = new DirectionsRouteDrawer(this, map);
        String drawingStatus = routeDrawer.drawRoute(start, end, routeMode);

        addPoints(start, end, draggable);
        if (DirectionsRouteDrawer.NO_POINTS.equals(drawingStatus)) {
            Toast.makeText(this, getString(R.string.no_points), Toast.LENGTH_SHORT).show();
        }
    }

    private void addPoints(LatLng start, LatLng end, Boolean draggable) {
        map.addMarker(new MarkerOptions().position(start).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory
                .HUE_GREEN)).draggable(draggable).title(START));
        map.addMarker(new MarkerOptions().position(end).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory
                .HUE_RED)).draggable(draggable).title(END));
    }

    private void getIncidentsFromDB() {
        ApiDBHelper dbHelper = new ApiDBHelper(this);
        allIncidents = dbHelper.getAllIncidents();
        for (Incident i : allIncidents) {
            drawIncidentsMarkers(i);
        }
        dbHelper.close();
    }

    private void drawIncidentsMarkers(Incident incident) {
        String street = incident.getAddress().getStreet();
        String houseNumber = incident.getAddress().getHouseNumber();
        String date = incident.getDate();
        String time = incident.getTime();
        String[] types = getResources().getStringArray(R.array.incidents_types_array);
        String incidentTYpe = types[incident.getIncidentType() - 1];
        LatLng point = incident.getAddress().getLocation();

        StringBuilder builder = new StringBuilder();
        builder.append(getString(R.string.street) + " " + noNull(street) + " " + noNull(houseNumber) + "\n");
        builder.append(getString(R.string.date) + noNull(date) + "\n");
        builder.append(getString(R.string.time) + noNull(time) + "\n");
        builder.append(getString(R.string.incident_type) + " " + incidentTYpe + "\n");

        String snippet = builder.toString();

        map.addCircle(new CircleOptions().center(point).fillColor(Color.argb(150, 250, 242, 12)).radius(80).strokeWidth(0));
        map.addMarker(new MarkerOptions().position(point).snippet(snippet).icon(BitmapDescriptorFactory.defaultMarker
                (BitmapDescriptorFactory
                .HUE_YELLOW)).draggable(false));

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = getLayoutInflater().inflate(R.layout.marker, null);

                TextView info = (TextView) v.findViewById(R.id.info);

                if (marker.getSnippet() != null) {
                    info.setText(marker.getSnippet());
                } else if (marker.getTitle() != null) {
                    info.setText(marker.getTitle());
                }
                return v;
            }
        });

    }

    private static String noNull(String nullable) {
        return nullable == null ? " " : nullable;
    }

    private void setLocationSettings() {
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.setMyLocationEnabled(true);
    }

    @Override
    public void onLocationChanged(Location location) {
        statusCheck();
        handleNewLocation(location);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        } else {
            handleNewLocation(location);
        }
    }

    private void handleNewLocation(Location location) {
        if (started) {
            double currentLatitude = location.getLatitude();
            double currentLongitude = location.getLongitude();
            LatLng currentLocation = new LatLng(currentLatitude, currentLongitude);
            checkIfInRangeOfIncident(currentLocation);
            map.animateCamera(CameraUpdateFactory.newLatLng(currentLocation));
        }
    }

    private void checkIfInRangeOfIncident(LatLng location) {
        if (location != null && allIncidents != null) {
            for(Incident incident : allIncidents) {
                float[] results = new float[20];
                Location.distanceBetween(location.latitude, location.longitude, incident.getAddress().getLatitude(), incident.getAddress().getLongitude(), results);
                if (results[0] < 100) {
                    Toast.makeText(this, getString(R.string.close_to_incident), Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.d("Conn fail with code: ", String.valueOf(connectionResult.getErrorCode()));
        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        setUpMap();
        setLocationSettings();
        drawRoute(this.startLocation, this.endLocation, true);
        addPointsDragging();
        statusCheck();
    }

    private void addPointsDragging() {
        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                statusCheck();
                map.clear();
                getIncidentsFromDB();
                if (START.equals(marker.getTitle())) {
                    startLocation = marker.getPosition();
                    drawRoute(startLocation, endLocation, true);
                } else if (END.equals(marker.getTitle())) {
                    endLocation = marker.getPosition();
                    drawRoute(startLocation, endLocation, true);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        googleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        checkConnection.close();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.startRouting:
                statusCheck();
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(startLocation, map.getMaxZoomLevel() - 3);
                map.animateCamera(cameraUpdate);
                map.clear();
                getIncidentsFromDB();
                drawRoute(this.startLocation, this.endLocation, false);
                started = true;
                break;
            case R.id.stopRouting:
                statusCheck();
                LatLng cameraTargetLatLng = new LatLng(54.3774983, 18.6142447);
                CameraUpdate restoreCamera = CameraUpdateFactory.newLatLngZoom(cameraTargetLatLng, 11);
                map.animateCamera(restoreCamera);
                started = false;
                break;
            default:
                break;
        }

    }

    public void polyline(PolylineOptions lineOptions) {
        this.route = lineOptions.getPoints();

    }
}

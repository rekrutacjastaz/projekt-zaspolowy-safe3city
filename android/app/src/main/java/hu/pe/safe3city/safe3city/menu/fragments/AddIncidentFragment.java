package hu.pe.safe3city.safe3city.menu.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import hu.pe.safe3city.safe3city.AddIncidentJSONDecoder;
import hu.pe.safe3city.safe3city.GetLocation;
import hu.pe.safe3city.safe3city.R;


public class AddIncidentFragment extends Fragment implements View.OnClickListener, RadioGroup
        .OnCheckedChangeListener {

    private static final String DATE_DIALOG = "DATE_DIALOG";
    private static final String TIME_DIALOG = "TIME_DIALOG";
    private static final int GET_LOCATION = 3;
    private static final String MAN = "m";
    private static final String WOMAN = "K";

    private Spinner spinner;
    private View rootView;
    private Integer incidentTypeID;
    private Integer year;
    private Integer month;
    private Integer day;
    private Integer hour;
    private Integer minute;
    private LatLng location;
    private Button dateButton;
    private Button timeButton;
    private String sex;
    private String dateString;
    private String timeString;
    private EditText street;
    private EditText houseNUmber;
    private EditText postalCode;
    private EditText city;
    private EditText age;
    private String userID;
    private String accountType;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.add_incident_fragment, container, false);
        spinner = (Spinner) rootView.findViewById(R.id.spinner);
        fillSpinnerWithIncidents();

        dateButton = (Button) rootView.findViewById(R.id.date_button);
        dateButton.setOnClickListener(this);

        timeButton = (Button) rootView.findViewById(R.id.time_button);
        timeButton.setOnClickListener(this);

        ImageButton getStartFromMap = (ImageButton) rootView.findViewById(R.id.getLocationFromMap);
        getStartFromMap.setOnClickListener(this);

        RadioGroup radioGroupSex = (RadioGroup) rootView.findViewById(R.id.radioGroupSex);
        radioGroupSex.setOnCheckedChangeListener(this);

        initializeEditTexts();

        Button addIncidentButton = (Button) rootView.findViewById(R.id.addIncidentButton);
        addIncidentButton.setOnClickListener(this);
        return rootView;
    }

    private void fillSpinnerWithIncidents() {
        String[] stringArray = rootView.getResources().getStringArray(R.array.incidents_types_array);
        final IncidentType types[] = new IncidentType[stringArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            types[i] = new IncidentType(i + 1, stringArray[i]);
        }

        ArrayAdapter<IncidentType> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                IncidentType itemAtPosition = (IncidentType) parent.getItemAtPosition(position);
                incidentTypeID = itemAtPosition.getIncidentTypeID();
                if (location != null)
                    Toast.makeText(getActivity(), "time: " + hour + ":" + minute + " date: " + day + "." + month + "." + year +
                            " incident: " + incidentTypeID + " " + location.toString(), Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initializeEditTexts() {
        street = (EditText) rootView.findViewById(R.id.street);
        houseNUmber = (EditText) rootView.findViewById(R.id.houseNumber);
        postalCode = (EditText) rootView.findViewById(R.id.postalCode);
        city = (EditText) rootView.findViewById(R.id.city);
        age = (EditText) rootView.findViewById(R.id.age);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.date_button:
                showDateChooser();
                break;
            case R.id.time_button:
                showTimeChooser();
                break;
            case R.id.getLocationFromMap:
                Intent getLocation = new Intent(getActivity(), GetLocation.class);
                startActivityForResult(getLocation, GET_LOCATION);
                break;
            case R.id.addIncidentButton:
                getLoggedUserData();
                addIncident();
                break;
            default:
                break;
        }
    }

    private void showDateChooser() {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setCallBack(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                setDate(year, monthOfYear, dayOfMonth);
            }
        });
        fragment.show(getFragmentManager(), DATE_DIALOG);
    }

    void setDate(int year, int month, int day) {
        this.year = year;
        this.month = month + 1;
        this.day = day;
        dateString = this.year + "-" + this.month + "-" + this.day;
        dateButton.setText(dateString);
    }

    private void showTimeChooser() {
        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setCallback(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                setTime(hourOfDay, minute);
            }
        });
        fragment.show(getFragmentManager(), TIME_DIALOG);
    }

    void setTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
        timeString = this.hour + ":" + this.minute;
        timeButton.setText(timeString);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.radioWoman:
                sex = WOMAN;
                break;
            case R.id.radioMan:
                sex = MAN;
                break;
            default:
                sex = null;
                break;
        }
    }

    private void addIncident() {
        if (userID == null || accountType == null) {
            Toast.makeText(getActivity(), getString(R.string.not_logged_in), Toast.LENGTH_SHORT).show();
            return;
        }
        if (location == null || incidentTypeID == null) {
            Toast.makeText(getActivity(), getString(R.string.fill_fields), Toast.LENGTH_SHORT).show();
            return;
        }
        if ("moderator".equals(accountType) || "admin".equals(accountType)) {
            userID = null;
        }
        JSONObject jIncident = new AddIncidentJSONDecoder().makeJSON(userID, incidentTypeID, sex, age.getText().toString(),
                dateString, timeString, location, street.getText().toString(), houseNUmber.getText().toString(),
                postalCode.getText().toString(), city.getText().toString());

        Log.d("jIncident", jIncident.toString());
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://188.226.171.110/api/api.php/add",
                jIncident, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
                try {
                    if ("CREATED".equals(response.getString("status"))) {
                        Toast.makeText(getActivity(), getString(R.string.incident_added), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.incident_not_added_error), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), getString(R.string.server_connection_error), Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(request);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.adding_incident));
        progressDialog.show();
    }

    private void getLoggedUserData() {
        this.userID = getUserID(getActivity());
        this.accountType = getAccountType(getActivity());
    }

    public static String getUserID(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(LoginFragment.LOGIN_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString("userID", null);
    }

    public static String getAccountType(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(LoginFragment.LOGIN_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString("accountType", null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == GET_LOCATION) {
            location = (LatLng) data.getExtras().get(GetLocation.LOCATION);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    static class IncidentType {

        private final String incidentName;
        private final Integer incidentTypeID;

        public IncidentType(Integer incidentTypeID, String incidentName) {
            this.incidentTypeID = incidentTypeID;
            this.incidentName = incidentName;
        }

        public Integer getIncidentTypeID() {
            return incidentTypeID;
        }

        public String getIncidentName() {
            return incidentName;
        }

        @Override
        public String toString() {
            return incidentName;
        }
    }
}

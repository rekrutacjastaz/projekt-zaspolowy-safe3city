package hu.pe.safe3city.safe3city.menu.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

import hu.pe.safe3city.safe3city.GetLocation;
import hu.pe.safe3city.safe3city.PlaceDetailsSetter;
import hu.pe.safe3city.safe3city.R;
import hu.pe.safe3city.safe3city.RoutingActivity;
import hu.pe.safe3city.safe3city.directions.PlaceDetailsDownloader;
import hu.pe.safe3city.safe3city.menu.helpers.PlacesAutoCompleteAdapter;

public class ChooseRouteFragment extends Fragment implements View.OnClickListener, PlaceDetailsSetter {

    public static final int START = 0;
    public static final int END = 1;
    public static final String MODE_WALKING = "walking";
    public static final String MODE_BICYCLING = "bicycling";

    private View rootView;
    private LatLng startLocation;
    private LatLng endLocation;
    private AutoCompleteTextView startText;
    private AutoCompleteTextView endText;
    private PlacesAutoCompleteAdapter autoCompleteAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.choose_route_fragment, container, false);

        ImageButton getStartFromMap = (ImageButton) rootView.findViewById(R.id.getStartFromMap);
        ImageButton getEndFromMap = (ImageButton) rootView.findViewById(R.id.getEndFromMap);
        Button drawRoute = (Button) rootView.findViewById(R.id.drawRoute);
        startText = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteStart);
        endText = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteEnd);
        addAutocomplete();

        setListeners(getStartFromMap, getEndFromMap, drawRoute);

        return rootView;
    }


    private void addAutocomplete() {
        autoCompleteAdapter = new PlacesAutoCompleteAdapter(getActivity(), R.layout.autocomplete_list_item);
        startText.setAdapter(autoCompleteAdapter);
        startText.setOnItemClickListener(startTextListener());
        endText.setAdapter(autoCompleteAdapter);
        endText.setOnItemClickListener(endTextListener());
    }

    private AdapterView.OnItemClickListener startTextListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                getPlaceDetails(adapterView, position, START);
            }
        };
    }

    private AdapterView.OnItemClickListener endTextListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                getPlaceDetails(adapterView, position, END);
            }
        };
    }

    private void getPlaceDetails(AdapterView<?> adapterView, int position, int pointType) {
        String str = (String) adapterView.getItemAtPosition(position);
        HashMap<String, String> idAndDesc = autoCompleteAdapter.getIdAndDesc();
        for (HashMap.Entry<String, String> entry : idAndDesc.entrySet()) {
            if (str.equals(entry.getValue())) {
                new PlaceDetailsDownloader(this, pointType).execute(entry.getKey());
            }
        }
    }

    private void setListeners(ImageButton getStartFromMap, ImageButton getEndFromMap, Button drawRoute) {
        getStartFromMap.setOnClickListener(this);
        getEndFromMap.setOnClickListener(this);
        drawRoute.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent getLocation = new Intent(getActivity(), GetLocation.class);
        switch (id) {
            case R.id.getStartFromMap:
                startActivityForResult(getLocation, START);
                break;
            case R.id.getEndFromMap:
                startActivityForResult(getLocation, END);
                break;
            case R.id.drawRoute:
                if (startLocation != null && endLocation != null) {
                    startRoutingActivity();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.no_points_selected), Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }

    private void startRoutingActivity() {
        Integer mapType = getMapType();
        String routeMode = getRouteMode();
        Intent routing = new Intent(getActivity(), RoutingActivity.class);
        routing.putExtra("startLocation", startLocation);
        routing.putExtra("endLocation", endLocation);
        routing.putExtra("mapType", mapType);
        routing.putExtra("routeMode", routeMode);
        startActivity(routing);
    }

    private int getMapType() {
        RadioGroup mapTypes = (RadioGroup) rootView.findViewById(R.id.mapTypesGroup);
        int mapTypeId = mapTypes.getCheckedRadioButtonId();

        if (mapTypeId == R.id.satelliteMap) {
            return GoogleMap.MAP_TYPE_SATELLITE;
        }
        return GoogleMap.MAP_TYPE_NORMAL;
    }

    private String getRouteMode() {
        RadioGroup routeModes = (RadioGroup) rootView.findViewById(R.id.routeModesGroup);
        int routeModeId = routeModes.getCheckedRadioButtonId();

        if (routeModeId == R.id.modeBicycling) {
            return MODE_BICYCLING;
        }
        return MODE_WALKING;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == START) {
                startLocation = (LatLng) data.getExtras().get(GetLocation.LOCATION);
                startText.setText(getString(R.string.from_map));
            }
            if (requestCode == END) {
                endLocation = (LatLng) data.getExtras().get(GetLocation.LOCATION);
                endText.setText(getString(R.string.from_map));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setStartLocation(LatLng startLocation) {
        this.startLocation = startLocation;
    }

    @Override
    public void setEndLocation(LatLng endLocation) {
        this.endLocation = endLocation;
    }

}



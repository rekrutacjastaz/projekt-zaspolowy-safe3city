package hu.pe.safe3city.safe3city;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

public class AddIncidentJSONDecoder {

    private String userID;
    private Integer incidentTypeID;
    private Double latitude;
    private Double longitude;
    private String sex;
    private String dateString;
    private String timeString;
    private String street;
    private String houseNumber;
    private String postalCode;
    private String city;
    private String age;

    public AddIncidentJSONDecoder() {

    }

    public AddIncidentJSONDecoder(String userID, Integer incidentTypeID, String sex, String age, String dateString, String timeString,
                                   LatLng location, String street, String houseNumber, String postalCode, String city) {

        this.userID = userID;
        this.incidentTypeID = incidentTypeID;
        this.latitude = location.latitude;
        this.longitude = location.longitude;
        this.sex = sex;
        this.dateString = dateString;
        this.timeString = timeString;
        this.street = street;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.age = age;
    }

    public JSONObject makeJSON(String userID, Integer incidentTypeID, String sex, String age, String dateString, String timeString,
                                LatLng location, String street, String houseNumber, String postalCode, String city) {

        this.userID = userID;
        this.incidentTypeID = incidentTypeID;
        this.latitude = location.latitude;
        this.longitude = location.longitude;
        this.sex = sex;
        this.dateString = dateString;
        this.timeString = timeString;
        this.street = street;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.age = age;
        return getJSON();
    }

    public JSONObject getJSON() {
        JSONObject jIncident = new JSONObject();
        try {
            jIncident.put("userID", getObjectOrJSONNull(userID));
            jIncident.put("incidentTypeID", incidentTypeID);
            jIncident.put("latitude", latitude);
            jIncident.put("longitude", longitude);
            jIncident.put("date", getObjectOrJSONNull(dateString));
            jIncident.put("time", getObjectOrJSONNull(timeString));
            jIncident.put("victimSex", getObjectOrJSONNull(sex));
            jIncident.put("victimAge", getObjectOrJSONNull(age));
            jIncident.put("street", getObjectOrJSONNull(street));
            jIncident.put("houseNumber", getObjectOrJSONNull(houseNumber));
            jIncident.put("postalCode", getObjectOrJSONNull(postalCode));
            jIncident.put("city", getObjectOrJSONNull(city));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jIncident;
    }

    private Object getObjectOrJSONNull(Object object) {
        return object != null ? object : JSONObject.NULL;
    }
}
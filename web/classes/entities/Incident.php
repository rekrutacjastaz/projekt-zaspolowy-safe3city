<?php
	class Incident { 
		public $id;
		public $userID;
		public $address;
		public $incidentType;
		public $date;
		public $time;
		public $victimSex;
		public $victimAge;
		
		public function __construct()
		{
			$this->address = new Address();
			$this->incidentType = new IncidentType();
		}
	} 
?>
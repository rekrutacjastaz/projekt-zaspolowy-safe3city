<?php
	class User { 
		public $id;
		public $login;
		public $email;
		public $passwordHash;
		public $isActive;
		public $activationHash;
		public $passwordResetHash;
		public $passwordResetTimestamp;
		public $rememberMeToken;
		public $failedLogins;
		public $lastFailedLogin;
		public $registrationDatetime; 
	} 
?>
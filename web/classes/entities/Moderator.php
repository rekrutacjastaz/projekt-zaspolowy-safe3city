<?php
	class Moderator { 
		public $id;
		public $login;
		public $email;
		public $passwordHash;
		public $passwordResetHash;
		public $passwordResetTimestamp;
		public $rememberMeToken;
		public $failedLogins;
		public $lastFailedLogin;
	} 
?>
<?php
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Moderator.php');
	require_once ('randomString.php');
	
	
	$db = new DAO();
		
		
	echo 'ADD MODERATOR TEST:<br>';
	$moderator = new Moderator();
	$moderator->login = randomString();
	$moderator->email = randomString();
	$moderator->passwordHash = randomString();
	$moderator->passwordResetHash = randomString();
	$moderator->passwordResetTimestamp = 123;
	$moderator->rememberMeToken = randomString();
	$moderator->failedLogins = 123;
	
	$db->moderator->add($moderator);
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'FIND MODERATOR TESTS:<br>';
	echo '-> by ID (11):<br>';
	$moderator = $db->moderator->findByID(11);
	if(is_null($moderator))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($moderator)) {
			echo "$key: $value<br>";
		}
	}
	
	echo '<br><br>-> by Login (Aot2GuRaRi):<br>';
	$moderator = $db->moderator->findByLogin('Aot2GuRaRi');
	if(is_null($moderator))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($moderator)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'GET ALL MODERATORS TEST:<br>';
	$all_moderators = $db->moderator->getAll();
	if(is_null($all_moderators))
		echo 'NOTHING FOUND';
	else {
		while (list(, $moderator) = each($all_moderators)) {
			while (list($key, $value) = each($moderator)) {
				echo "$key: $value<br>";
			}
			echo '<br>';
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'DELETE MODERATOR TESTS:<br>';
	echo '-> by ID (14) + try to find:<br>';
	$db->moderator->deleteByID(14);
	$moderator = $db->moderator->findByID(14);
	if(is_null($moderator))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	
	echo '<br><br>-> by Login (9pbuork8CH) + try to find:<br>';
	$db->moderator->deleteByLogin('9pbuork8CH');
	$moderator = $db->moderator->findByLogin('9pbuork8CH');
	if(is_null($moderator))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';

	
	/*
	echo 'DELETE ALL MODERATORS TEST + try get all moderators:<br>';
	$db->moderator->deleteAll();

	$all_moderators = $db->moderator->getAll();
	if(is_null($all_moderators))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	*/
	
	
	echo 'UPDATE MODERATOR TEST:<br>';
	echo '-> before (ID 18):<br>';
	$moderator = $db->moderator->findByID(18);
	if(is_null($moderator))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($moderator)) {
			echo "$key: $value<br>";
		}
		
		$moderator->login = 'abc';
		$moderator->passwordHash = 'abc';
		$moderator->passwordResetHash = 'abc';
		$moderator->passwordResetTimestamp = 111;
		$moderator->rememberMeToken = 'abc';
		$moderator->lastFailedLogin = '2001-01-01 12:34:56';
		
		$db->moderator->update($moderator);
		
		echo '<br><br>-> after (ID 18):<br>';
		$moderator = $db->moderator->findByID(18);
		while (list($key, $value) = each($moderator)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo '<br><br>DONE';
?>
<?php
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Admin.php');
	require_once ('randomString.php');
	
	
	$db = new DAO();
		
		
	echo 'ADD ADMIN TEST:<br>';
	$admin = new Admin();
	$admin->login = randomString();
	$admin->email = randomString();
	$admin->passwordHash = randomString();
	$admin->passwordResetHash = randomString();
	$admin->passwordResetTimestamp = 123;
	$admin->rememberMeToken = randomString();
	$admin->lastFailedLogin = 123;
	$db->admin->add($admin);
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'FIND ADMIN TESTS:<br>';
	echo '-> by ID (1):<br>';
	$admin = $db->admin->findByID(1);
	if(is_null($admin))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($admin)) {
			echo "$key: $value<br>";
		}
	}
	
	
	echo '<br><br>-> by Login (BQnGaSO4sL):<br>';
	$admin = $db->admin->findByLogin('BQnGaSO4sL');
	if(is_null($admin))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($admin)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'GET ALL ADMINS TEST:<br>';
	$all_admins = $db->admin->getAll();
	if(is_null($all_admins))
		echo 'NOTHING FOUND';
	else {
		while (list(, $admin) = each($all_admins)) {
			while (list($key, $value) = each($admin)) {
				echo "$key: $value<br>";
			}
			echo '<br>';
		}
	}
	echo '<br>------------------------------------<br><br>';

	
	echo 'DELETE ADMIN TESTS:<br>';
	echo '-> by ID (10) + try to find:<br>';
	$db->admin->deleteByID(10);
	$admin = $db->admin->findByID(10);
	if(is_null($admin))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	
	echo '<br><br>-> by Login (3vvnCPaT7q) + try to find:<br>';
	$db->admin->deleteByLogin('3vvnCPaT7q');
	$admin = $db->admin->findByLogin('3vvnCPaT7q');
	if(is_null($admin))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	
	
	/*
	echo 'DELETE ALL ADMINS TEST + try get all admins:<br>';
	$db->admin->deleteAll();

	$all_admins = $db->admin->getAll();
	if(is_null($all_admins))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	*/
	
	
	echo 'UPDATE ADMIN TEST:<br>';
	echo '-> before (ID 20):<br>';
	$admin = $db->admin->findByID(20);
	if(is_null($admin))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($admin)) {
			echo "$key: $value<br>";
		}
		
		$admin->login = 'abc';
		$admin->passwordHash = 'abc';
		$admin->passwordResetHash = 'abc';
		$admin->passwordResetTimestamp = 111;
		$admin->rememberMeToken = 'abc';
		$admin->failedLogins = 111;
		$admin->lastFailedLogin = '2001-01-01 12:34:56';
		
		$db->admin->update($admin);
		
		echo '<br><br>-> after (ID 20):<br>';
		$admin = $db->admin->findByID(20);
		while (list($key, $value) = each($admin)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'EXTRA ADMIN FUNCTIONS TEST (dependent on the previous):<br>';
	$admin = $db->admin->findByID(20);
	if(is_null($admin))
		echo 'NOT FOUND';
	else {
		$db->admin->changeLogin($admin, 'new login');
		$admin = $db->admin->findByID(20);
		while (list($key, $value) = each($admin)) {
			echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';


	echo '<br><br>DONE';
?>
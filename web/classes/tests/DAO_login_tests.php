<?php
	require ('../database/DAO.php');
	require ('../entities/Account.php');
	
	
	$db = new DAO();
	
	
	echo 'TRY TO LOG IN TESTS:<br>';
	echo '-> as admin:<br>';
	$account = $db->login->tryLogIn('U9q58A5C87', 'HseMd3RGJz');
	if($account->accountType === 'admin')
		echo 'SUCCESS';
	else 
		echo 'FAILURE!';
		
	echo '<br><br>-> as user:<br>';
	$account = $db->login->tryLogIn('e2y8aYRkfS', 'y2kIdZJXXo');
	if($account->accountType === 'user')
		echo 'SUCCESS';
	else 
		echo 'FAILURE!';
		
	echo '<br><br>-> as moderator:<br>';
	$account = $db->login->tryLogIn('WPkbWVADuh', 'n1rAz4Or2G');
	if($account->accountType === 'moderator')
		echo 'SUCCESS';
	else 
		echo 'FAILURE!';
		
	echo '<br><br>-> with wrong password hash:<br>';
	$account = $db->login->tryLogIn('WPkbWVADuh', 'abc123');
	if(is_null($account))
		echo 'SUCCESS';
	else 
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	
	echo '<br><br>DONE';
?>


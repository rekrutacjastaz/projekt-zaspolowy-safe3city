<?php
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Address.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/IncidentType.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Incident.php');
	require_once ('randomString.php');
	
	
	$db = new DAO();
	
	
	echo 'ADD INCIDENT TESTS:<br>';
	echo '-> hardcoded ids (DON\'T DO THAT!!!)<br>';
	$incident = new Incident();
	$incident->userID = 32;
	$incident->address->id = 9; 
	$incident->incidentType->id = 27;
	$incident->date = '2001-01-01';
	$incident->time = '01:01';
	$incident->victimSex = 'male';
	$incident->victimAge = 18;
	$db->incident->add($incident);

	echo '<br><br>-> only few field set<br>';
	$incident = new Incident();
	$incident->address->street = randomString();
	$incident->incidentType->name = randomString();
	$incident->date = '2001-01-01';
	$incident->time = '01:01';
	$incident->victimSex = 'male';
	$incident->victimAge = 19;
	$db->incident->add($incident);

	echo '<br><br>-> existing address and incident type<br>';
	$incident = new Incident();
	$incident->address = $db->address->findByID(9);
	$incident->incidentType = $db->incidentType->findByID(27);
	$incident->date = '2001-01-01';
	$incident->time = '01:01';
	$incident->victimSex = 'male';
	$incident->victimAge = 20;
	$db->incident->add($incident);
	
	echo '<br><br>-> new address and incident type objects<br>';
	$address = new Address();
	$address->street = 'my street';
	$address->houseNumber = 'random number';
	$address->postalCode = '12-345';
	$address->city = randomString();
	$address->latitude = 12.345;
	$address->longitude = 12.345;
	
	$incidentType = new IncidentType();
	$incidentType->name = 'my name';
	$incidentType->type = 'my type';
	$incidentType->weight = 123;
	
	$incident = new Incident();
	$incident->address = $address;
	$incident->incidentType = $incidentType;
	$incident->date = '2001-01-01';
	$incident->time = '01:01';
	$incident->victimSex = 'male';
	$incident->victimAge = 21;
	$db->incident->add($incident);
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'FIND INCIDENT TESTS:<br>';
	echo '-> by ID (38):<br>';
	$incident = $db->incident->findByID(38);
	if(is_null($incident))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($incident)) {
			if($key === 'address') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			elseif($key === 'incidentType') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			else echo "$key: $value<br>";
		}
	}
	
	echo '<br><br>-> by incident name (pX5rscLkdy):<br>';
	$incidents = $db->incident->findByIncidentName('pX5rscLkdy');
	if(is_null($incident))
		echo 'NOTHING FOUND';
	else {
		while (list(, $incident) = each($incidents)) {
			while (list($key, $value) = each($incident)) {
				if($key === 'address') {
					echo "$key:<br>";
					while (list($key2, $value2) = each($value))
						echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
				}
				elseif($key === 'incidentType') {
					echo "$key:<br>";
					while (list($key2, $value2) = each($value))
						echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
				}
				else echo "$key: $value<br>";
			}
			echo '<br>';
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'GET ALL INCIDENTS TEST (approved):<br>';
	$all_incidents = $db->incident->getAll();
	if(is_null($all_incidents))
		echo 'NOTHING FOUND';
	else {
		while (list(, $incident) = each($all_incidents)) {
			while (list($key, $value) = each($incident)) {
				if($key === 'address') {
					echo "$key:<br>";
					while (list($key2, $value2) = each($value))
						echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
				}
				elseif($key === 'incidentType') {
					echo "$key:<br>";
					while (list($key2, $value2) = each($value))
						echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
				}
				else echo "$key: $value<br>";
			}
			echo '<br>';
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'GET ALL INCIDENTS TEST (unapproved):<br>';
	$all_incidents = $db->incident->getUnapproved();
	if(is_null($all_incidents))
		echo 'NOTHING FOUND';
	else {
		while (list(, $incident) = each($all_incidents)) {
			while (list($key, $value) = each($incident)) {
				if($key === 'address') {
					echo "$key:<br>";
					while (list($key2, $value2) = each($value))
						echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
				}
				elseif($key === 'incidentType') {
					echo "$key:<br>";
					while (list($key2, $value2) = each($value))
						echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
				}
				else echo "$key: $value<br>";
			}
			echo '<br>';
		}
	}
	echo '<br>------------------------------------<br><br>';

	
	echo 'DELETE INCIDENT TEST:<br>';
	echo '-> by ID (142) + try to find:<br>';
	$db->incident->deleteByID(142);
	$incident = $db->incident->findByID(142);
	if(is_null($incident))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	
	
	/*
	echo 'DELETE ALL INCIDENTS TEST + try get all incidents:<br>';
	$db->incident->deleteAll();

	$all_incidents = $db->incident->getAll();
	if(is_null($all_incidents))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';
	*/
	
	echo 'UPDATE INCIDENT TESTS:<br>';
	echo 'change only incident fields<br>';
	echo '-> before (ID 186):<br>';
	$incident = $db->incident->findByID(186);
	if(is_null($incident))
		echo 'NOT FOUND';
	else {
		while (list($key, $value) = each($incident)) {
			if($key === 'address') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			elseif($key === 'incidentType') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			else echo "$key: $value<br>";
		}
		
		$incident->userID = 123;
		$incident->date = '1234-12-12';
		$incident->time = '11:11';
		$incident->victimSex = 'female';
		$incident->victimAge = 123;
		
		$db->incident->update($incident);
		
		echo '<br><br>-> after (ID 186):<br>';
		$incident = $db->incident->findByID(186);
		while (list($key, $value) = each($incident)) {
			if($key === 'address') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			elseif($key === 'incidentType') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			else echo "$key: $value<br>";
		}
	
	
		echo '<br><br>change address and incidentType fields (dependent on the previous):<br>';
		echo 'DANGEROUS - READ DOCUMENTATION BEFORE USE!!!<br>';
		
		$incident->address->street = 'dangerous change';
		$incident->incidentType->name = 'dangerous change';
		
		$db->incident->update($incident);
		
		echo '-> after dangerous change:<br>';
		$incident = $db->incident->findByID(186);
		while (list($key, $value) = each($incident)) {
			if($key === 'address') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			elseif($key === 'incidentType') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			else echo "$key: $value<br>";
		}
		
		
		echo '<br><br>change address and incidentType objects (dependent on the previous):<br>';
		
		$newAddress = new Address();
		$newAddress->street = 'new object';
		
		$newIncidentType = new IncidentType();
		$newIncidentType->name = 'new object';
		
		$incident->address = $newAddress;
		$incident->incidentType = $newIncidentType;
		
		$db->incident->update($incident);
		
		echo '-> after:<br>';
		$incident = $db->incident->findByID(186);
		while (list($key, $value) = each($incident)) {
			if($key === 'address') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			elseif($key === 'incidentType') {
				echo "$key:<br>";
				while (list($key2, $value2) = each($value))
					echo "&nbsp;&nbsp;&nbsp;&nbsp;$key2: $value2<br>";
			}
			else echo "$key: $value<br>";
		}
	}
	echo '<br>------------------------------------<br><br>';
	
	
	echo 'ACCEPT INCIDENT TEST:<br>';
	$incident = $db->incident->findByID(201);
	$db->incident->accept($incident);

	$incident = $db->incident->findByID(201);
	if(is_null($incident->userID))
		echo 'SUCCESS';
	else
		echo 'FAILURE!';
	echo '<br>------------------------------------<br><br>';

	
	echo '<br><br>DONE';
?>
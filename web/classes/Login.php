<?php

require_once('translations/pl.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/DAO.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/User.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Admin.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Moderator.php');
require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/entities/Account.php');

class Login
{
   
    private $db_connection = null;
    
    private $id = null;
    
    private $name = "";
   
    private $email = "";
    
    private $logged_in = false;
   
    private $password_reset_link_is_valid  = false;
 
    private $password_reset_was_successful = false;
  
    public $errors = array();
   
    public $messages = array();
	
	public $accountType = null;
	
	
    
    public function __construct()
    {
       
        session_start();

		
        if (isset($_GET["logout"])) {
            $this->doLogout();
       
        } elseif (!empty($_SESSION['name']) && ($_SESSION['logged_in'] == 1)) {
            $this->loginWithSessionData();
          
            if (isset($_POST["edit_submit_name"])) {
                
                $this->editName($_POST['name']);
           
            } elseif (isset($_POST["edit_submit_email"])) {
               
                $this->editEmail($_POST['email']);
            
            } elseif (isset($_POST["edit_submit_password"])) {
                
                $this->editPassword($_POST['password_old'], $_POST['password_new'], $_POST['password_repeat']);
            }
       
        } elseif (isset($_COOKIE['rememberme'])) {
            $this->loginWithCookieData();
        
        } elseif (isset($_POST["login"])) {
            if (!isset($_POST['rememberme'])) {
                $_POST['rememberme'] = null;
            }
            $this->loginWithPostData($_POST['name'], $_POST['password'], $_POST['rememberme']);
        }
      
        if (isset($_POST["request_password_reset"]) && isset($_POST['name'])) {
            $this->setPasswordResetDatabaseTokenAndSendMail($_POST['name']);
        } elseif (isset($_GET["name"]) && isset($_GET["verification_code"])) {
            $this->checkIfEmailVerificationCodeIsValid($_GET["name"], $_GET["verification_code"]);
        } elseif (isset($_POST["submit_new_password"])) {
            $this->editNewPassword($_POST['name'], $_POST['password_reset_hash'], $_POST['password_new'], $_POST['password_repeat']);
        }
    }

    private function loginWithSessionData()
    {
        $this->name = $_SESSION['name'];
        $this->email = $_SESSION['email'];

       
        $this->logged_in = true;
    }

   
    private function loginWithCookieData()
    {
		
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
		$account = new Account();
		
        if (isset($_COOKIE['rememberme'])) {
           
            list ($id, $token, $hash) = explode(':', $_COOKIE['rememberme']);
          
            if ($hash == hash('sha256', $id . ':' . $token . COOKIE_SECRET_KEY) && !empty($token)) {
                
               
					$check = $db->user->findByID($id);
					if(is_null($check)) {						
						$check = $db->admin->findByID($id);
						
						if(is_null($check)) {							
							$check = $db->moderator->findByID($id);							
						}
					}

                    if (!is_null($check->id) && $check->rememberMeToken == $token && $check->rememberMeToken != NULL ) {
                       
                        $_SESSION['id'] = $check->id;
                        $_SESSION['name'] = $check->login;
                        $_SESSION['email'] = $check->email;
                        $_SESSION['logged_in'] = 1;

                      
                        $this->id =  $check->id;
                        $this->name = $check->login;
                        $this->email = $check->email;
                        $this->logged_in = true;
						
                      
                        $this->newRememberMeCookie();
                        return true;
                    }
                
            }
           
            $this->deleteRememberMeCookie();
            $this->errors[] = MESSAGE_COOKIE_INVALID;
        }
        return false;
    }

    
    private function loginWithPostData($name, $password, $rememberme)
    {
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
		
		 $result_row = $db->user->findByLogin($name);
		
        if (empty($name)) {
            $this->errors[] = MESSAGE_USERNAME_EMPTY;
        } else if (empty($password)) {
            $this->errors[] = MESSAGE_PASSWORD_EMPTY;
	
        } else {
            
            if (!filter_var($name, FILTER_VALIDATE_EMAIL)) {
       
                $result_row = $db->user->findByLogin($name);
				if(!is_null($result_row)) {
					if ($result_row->isActive != 1) {
						$this->errors[] = MESSAGE_ACCOUNT_NOT_ACTIVATED;
						return;
					}
				}
				else if(is_null($result_row)){					
					 $result_row = $db->admin->findByLogin($name);
					 
					 if(is_null($result_row)) {							
						$result_row = $db->moderator->findByLogin($name);							
					}	 
				}
            
            } else{
               
                $result_row = $db->user->findByLogin($name);
				if(!is_null($result_row)) {
					if ($result_row->isActive != 1) {
						$this->errors[] = MESSAGE_ACCOUNT_NOT_ACTIVATED;
						return;
					}
				}
				else if(is_null($result_row)){					
					$result_row = $db->admin->findByLogin($name);
					
					if(is_null($result_row)) {						
							$result_row = $db->moderator->findByLogin($name);							
					}					 
				}
            }

            
            if (is_null($result_row)) {
                
                $this->errors[] = MESSAGE_LOGIN_FAILED;
				
            } else if (($result_row->failedLogins >= 3) && ($result_row->lastFailedLogin > (time() - 30))) {
				
                $this->errors[] = MESSAGE_PASSWORD_WRONG_3_TIMES;
            
            } else if (! password_verify($password, $result_row->passwordHash)) {
                
				$result_row->failedLogins = $result_row->failedLogins +1;
				$result_row->lastFailedLogin = date("Y-m-d H:i:s");
				
                $this->Update($name,$result_row);

                $this->errors[] = MESSAGE_PASSWORD_WRONG;

            } else {
                
                $_SESSION['id'] = $result_row->id;
                $_SESSION['name'] = $result_row->login;
                $_SESSION['email'] = $result_row->email;
                $_SESSION['logged_in'] = 1;

                
                $this->id = $result_row->id;
                $this->name = $result_row->login;
                $this->email = $result_row->email;
                $this->logged_in = true;

                
				$result_row->failedLogins = 0;
				$result_row->lastFailedLogin = "0000-00-00 00:00:00";
			
			
				$this->Update($name,$result_row);
				
				$check = $this->findByLogin($name);
				
				$account = $db->login->tryLogIn($check->login, $check->passwordHash);
				
				$_SESSION['accountType'] = $account->accountType;
				
				header("Location: http://188.226.171.110/index.php"); 
					 
                if (isset($rememberme)) {
                    $this->newRememberMeCookie();
                } else {                  
                    $this->deleteRememberMeCookie();
                }
            
                if (defined('HASH_COST_FACTOR')) {
                   
                    if (password_needs_rehash($result_row->passwordHash, PASSWORD_DEFAULT, array('cost' => HASH_COST_FACTOR))) {
                    
                        $password_hash = password_hash($password, PASSWORD_DEFAULT, array('cost' => HASH_COST_FACTOR));

						$result_row->passwordHash = $password_hash;
						
						 $this->Update($name,$result_row);
                    }
                }
            }
        }
    }

   
    private function newRememberMeCookie()
    {
			$db = new DAO();
			$user = new User();
			$admin = new Admin();
			$moderator = new Moderator();
			
			$check = $this->findByLogin($_SESSION['name']);
			
            $random_token_string = hash('sha256', mt_rand());
			
			$check->rememberMeToken =  $random_token_string;
			
			$this->Update($_SESSION['name'],$check);

            $cookie_string_first_part = $_SESSION['id'] . ':' . $random_token_string;
            $cookie_string_hash = hash('sha256', $cookie_string_first_part . COOKIE_SECRET_KEY);
            $cookie_string = $cookie_string_first_part . ':' . $cookie_string_hash;

            
            setcookie('rememberme', $cookie_string, time() + COOKIE_RUNTIME, "/", COOKIE_DOMAIN);
        
    }

    private function deleteRememberMeCookie()
    {
			$db = new DAO();
			$user = new User();
			$admin = new Admin();
			$moderator = new Moderator();
			
			$check = $this->findByLogin($_SESSION['name']);
			
			$check->rememberMeToken =  NULL;
			
			$this->Update($_SESSION['name'],$check);

			setcookie('rememberme', false, time() - (3600 * 3650), '/', COOKIE_DOMAIN);
    }

    
    public function doLogout()
    {
        $this->deleteRememberMeCookie();

        $_SESSION = array();
        session_destroy();
		header('Location: http://188.226.171.110/');
		exit;

        $this->logged_in = false;
        $this->messages[] = MESSAGE_LOGGED_OUT;
    }

    
    public function LoggedIn()
    {
        return $this->logged_in;
    }

    
    public function editName($name)
    {
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
			
        $name = substr(trim($name), 0, 64);

        if (!empty($name) && $name == $_SESSION['name']) {
            $this->errors[] = MESSAGE_USERNAME_SAME_LIKE_OLD_ONE;

       
        } elseif (empty($name) || !preg_match("/^(?=.{6,64}$)[a-zA-Z][a-zA-Z0-9]*(?: [a-zA-Z0-9]+)*$/", $name)) {
            $this->errors[] = MESSAGE_USERNAME_INVALID;

        } else {
            
            $result_row =  $this->findByLogin($name);
			
            if (!is_null($result_row)) {
                $this->errors[] = MESSAGE_USERNAME_EXISTS;
            } else {
               
				$result_row =  $this->findByLogin($_SESSION['name']);

				$result_row->login = $name;
				
				$this->Update($_SESSION['name'],$result_row);
				
				$result_row =  $this->findByLogin($name);
				
                if (!is_null($result_row)) {
                    $_SESSION['name'] = $name;
                    $this->messages[] = MESSAGE_USERNAME_CHANGED_SUCCESSFULLY . $name;
                } else {
                    $this->errors[] = MESSAGE_USERNAME_CHANGE_FAILED;
                }
            }
        }
    }

    
    public function editEmail($email)
    {
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
	   
        $email = substr(trim($email), 0, 64);

        if (!empty($email) && $email == $_SESSION["email"]) {
            $this->errors[] = MESSAGE_EMAIL_SAME_LIKE_OLD_ONE;
        
        } elseif (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = MESSAGE_EMAIL_INVALID;

        } else {
			
            $result_row = $db->user->findByEmail($email);
			if(is_null($result_row)){	
				$result_row = $db->admin->findByEmail($email);
					
				if(is_null($result_row)){
					$result_row = $db->moderator->findByEmail($email);	
				}	
			}
				
            if (!is_null($result_row)) {
                $this->errors[] = MESSAGE_EMAIL_ALREADY_EXISTS;
            } else {
                
				$result_row =  $this->findByLogin($_SESSION['name']);

				$result_row->email = $email;
				
				$this->Update($_SESSION['name'],$result_row);	
				
				$result_row = $db->user->findByEmail($email);
				if(is_null($result_row)){
					$result_row = $db->admin->findByEmail($email);
						
					if(is_null($result_row)){
						$result_row = $db->moderator->findByEmail($email);
					}	
				}

                if (!is_null($result_row)) {
                    $_SESSION['email'] = $email;
                    $this->messages[] = MESSAGE_EMAIL_CHANGED_SUCCESSFULLY . $email;
                } else {
                    $this->errors[] = MESSAGE_EMAIL_CHANGE_FAILED;
                }
            }
        }
    }

   
    public function editPassword($password_old, $password_new, $password_repeat)
    {
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
		
        if (empty($password_new) || empty($password_repeat) || empty($password_old)) {
            $this->errors[] = MESSAGE_PASSWORD_EMPTY;
        
        } elseif ($password_new !== $password_repeat) {
            $this->errors[] = MESSAGE_PASSWORD_BAD_CONFIRM;
       
        } elseif (strlen($password_new) < 6) {
            $this->errors[] = MESSAGE_PASSWORD_TOO_SHORT;

        
        } else {
           
			$result_row =  $this->findByLogin($_SESSION['name']);

            if (!is_null($result_row)) {

               
                if (password_verify($password_old, $result_row->passwordHash)) {

                    $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);

                    
                    $password_hash = password_hash($password_new, PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));

					$result_row->passwordHash = $password_hash;
					
					$this->Update($_SESSION['name'],$result_row);
                   
					$result_row =  $this->findByLogin($_SESSION['name']);

                    if ($result_row->passwordHash == $password_hash) {
                        $this->messages[] = MESSAGE_PASSWORD_CHANGED_SUCCESSFULLY;
                    } else {
                        $this->errors[] = MESSAGE_PASSWORD_CHANGE_FAILED;
                    }
                } else {
                    $this->errors[] = MESSAGE_OLD_PASSWORD_WRONG;
                }
            } else {
                $this->errors[] = MESSAGE_USER_DOES_NOT_EXIST;
            }
        }
    }

   
    public function setPasswordResetDatabaseTokenAndSendMail($name)
    {
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
		
        $name = trim($name);

        if (empty($name)) {
            $this->errors[] = MESSAGE_USERNAME_EMPTY;

        } else {
           
            $temporary_timestamp = time();
           
            $password_reset_hash = sha1(uniqid(mt_rand(), true));
			
			$result_row =  $this->findByLogin($name);

            if (!is_null($result_row)) {

                $result_row->passwordResetHash = $password_reset_hash;
				$result_row->passwordResetTimestamp = $temporary_timestamp;
				
				$this->Update($name,$result_row);
           
				$result_row =  $this->findByLogin($name);
				
                if ($result_row->passwordResetHash == $password_reset_hash) {
                   
                    $this->sendPasswordResetMail($name, $result_row->email, $password_reset_hash);
                    return true;
                } else {
                    $this->errors[] = MESSAGE_DATABASE_ERROR;
                }
            } else {
                $this->errors[] = MESSAGE_USER_DOES_NOT_EXIST;
            }
        }
       
        return false;
    }

    
    public function sendPasswordResetMail($name, $email, $password_reset_hash)
    {
        $mail = new PHPMailer;

        
        if (EMAIL_USE_SMTP) {
           
            $mail->IsSMTP();
           
            $mail->SMTPAuth = EMAIL_SMTP_AUTH;
            
            if (defined(EMAIL_SMTP_ENCRYPTION)) {
                $mail->SMTPSecure = EMAIL_SMTP_ENCRYPTION;
            }
            
            $mail->Host = EMAIL_SMTP_HOST;
            $mail->Username = EMAIL_SMTP_USERNAME;
            $mail->Password = EMAIL_SMTP_PASSWORD;
            $mail->Port = EMAIL_SMTP_PORT;
        } else {
            $mail->IsMail();
        }

        $mail->From = EMAIL_PASSWORDRESET_FROM;
        $mail->FromName = EMAIL_PASSWORDRESET_FROM_NAME;
        $mail->AddAddress($email);
        $mail->Subject = EMAIL_PASSWORDRESET_SUBJECT;

        $link    = EMAIL_PASSWORDRESET_URL.'?name='.urlencode($name).'&verification_code='.urlencode($password_reset_hash);
        $mail->Body = EMAIL_PASSWORDRESET_CONTENT . ' ' . $link;

        if(!$mail->Send()) {
            $this->errors[] = MESSAGE_PASSWORD_RESET_MAIL_FAILED;
            return false;
        } else {
            $this->messages[] = MESSAGE_PASSWORD_RESET_MAIL_SUCCESSFULLY_SENT;
            return true;
        }
    }

    
    public function checkIfEmailVerificationCodeIsValid($name, $verification_code)
    {
		
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
		
        $name = trim($name);

        if (empty($name) || empty($verification_code)) {
            $this->errors[] = MESSAGE_LINK_PARAMETER_EMPTY;
        } else {
            
			$result_row =  $this->findByLogin($name);

            if (!is_null($result_row) && $result_row->passwordResetHash == $verification_code) {

                $timestamp_one_hour_ago = time() - 3600; 

                if ($result_row->passwordResetTimestamp > $timestamp_one_hour_ago) {
                    
                    $this->password_reset_link_is_valid = true;
                } else {
                    $this->errors[] = MESSAGE_RESET_LINK_HAS_EXPIRED;
                }
            } else {
                $this->errors[] = MESSAGE_USER_DOES_NOT_EXIST;
            }
        }
    }

    
    public function editNewPassword($name, $password_reset_hash, $password_new, $password_repeat)
    {
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
       
        $name = trim($name);

        if (empty($name) || empty($password_reset_hash) || empty($password_new) || empty($password_repeat)) {
            $this->errors[] = MESSAGE_PASSWORD_EMPTY;
       
        } else if ($password_new !== $password_repeat) {
            $this->errors[] = MESSAGE_PASSWORD_BAD_CONFIRM;
       
        } else if (strlen($password_new) < 6) {
            $this->errors[] = MESSAGE_PASSWORD_TOO_SHORT;
        
        } else {
          
            $hash_cost_factor = (defined('HASH_COST_FACTOR') ? HASH_COST_FACTOR : null);

           
            $password_hash = password_hash($password_new, PASSWORD_DEFAULT, array('cost' => $hash_cost_factor));
			
			$result_row =  $this->findByLogin($name);

			$result_row->passwordHash = $password_hash;
			$result_row->passwordResetHash = NULL;
			$result_row->passwordResetTimestamp = NULL;
			
			$this->Update($name,$result_row);
   
			$result_row =  $this->findByLogin($name);
            
            if ($result_row->passwordHash == $password_hash) {
                $this->password_reset_was_successful = true;
                $this->messages[] = MESSAGE_PASSWORD_CHANGED_SUCCESSFULLY;
            } else {
                $this->errors[] = MESSAGE_PASSWORD_CHANGE_FAILED;
            }
        }
    }

    
    public function passwordResetLinkIsValid()
    {
        return $this->password_reset_link_is_valid;
    }

    public function passwordResetWasSuccessful()
    {
        return $this->password_reset_was_successful;
    }

	private function findByLogin($name)
	{
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
		
		$result_row = $db->user->findByLogin($name);
		if(is_null($result_row)){
			$result_row = $db->admin->findByLogin($name);
			
			if(is_null($result_row)){
				$result_row = $db->moderator->findByLogin($name);
			}
		}
		
		return $result_row;
	}
	
	private function Update($name,$result_row)
	{
		$db = new DAO();
		$user = new User();
		$admin = new Admin();
		$moderator = new Moderator();
		
		if(!is_null($db->user->findByLogin($name))){					
			$db->user->update($result_row);							
		} else if(!is_null($db->admin->findByLogin($name))){							
			$db->admin->update($result_row);							
		} else if(!is_null($db->moderator->findByLogin($name))) {					
			$db->moderator->update($result_row);							
		}
	}
    
    public function getUsername()
    {
        return $this->name;
    }
}

<?php
	class AddressManager {
	
		private $db;
		
		private function mapDBAddressToAddress($DBAddress) {
			
			$address = new Address();
			
			$address->id = $DBAddress->id;
			$address->street = $DBAddress->street;
			$address->houseNumber = $DBAddress->house_number;
			$address->postalCode = $DBAddress->postal_code;
			$address->city = $DBAddress->city;
			$address->latitude = $DBAddress->latitude;
			$address->longitude = $DBAddress->longitude;
			
			return $address;
		}
		
		private function fillDBAddressWithData($DBAddress, $modelAddress) {
		
			$DBAddress->street = $modelAddress->street;
			$DBAddress->house_number = $modelAddress->houseNumber;
			$DBAddress->postal_code = $modelAddress->postalCode;
			$DBAddress->city = $modelAddress->city;
			$DBAddress->latitude = $modelAddress->latitude;
			$DBAddress->longitude = $modelAddress->longitude;
			
			return $DBAddress;
		}
		
		
		public function __construct($connection)
		{
			$this->db = $connection;
		}
		
		
		public function add($newAddress) {
		
			$address = new DB\SQL\Mapper($this->db, 'address');
			
			$address->street = $newAddress->street;
			$address->house_number = $newAddress->houseNumber;
			$address->postal_code = $newAddress->postalCode;
			$address->city = $newAddress->city;
			$address->latitude =$newAddress->latitude;
			$address->longitude =$newAddress->longitude;
			
			$address->save();
			return $address->id;
		}
		
		public function findByID($id) {
			
			$retrievedAddress = new DB\SQL\Mapper($this->db, 'address');
			$retrievedAddress->load(array('id=?', $id));
			
			if(is_null($retrievedAddress->id))
				return null;
			
			return $this->mapDBAddressToAddress($retrievedAddress);
		}
		
		public function getAll() {
		
			$retrievedAddress = new DB\SQL\Mapper($this->db, 'address');
			$retrievedAddress->load();
			
			if(is_null($retrievedAddress->id))
				return null;
			
			$addresses = array();
			
			do { 
				array_push($addresses, $this->mapDBAddressToAddress($retrievedAddress)); 
				
			} while($retrievedAddress->next());
			
			return $addresses;
		}
		
		public function deleteByID($id) {
		
			$addressToDelete = new DB\SQL\Mapper($this->db, 'address');
			$addressToDelete->load(array('id=?', $id));
			
			$addressToDelete->erase();
		}
	
		public function deleteAll() {
		
			$this->db->exec('DELETE FROM address');
		}
		
		public function update($address) {
		
			$addressToUpdate = new DB\SQL\Mapper($this->db, 'address');
			$addressToUpdate->load(array('id=?', $address->id));
			$addressToUpdate = $this->fillDBAddressWithData($addressToUpdate, $address);
			
			$addressToUpdate->save();
		}
	}
?>
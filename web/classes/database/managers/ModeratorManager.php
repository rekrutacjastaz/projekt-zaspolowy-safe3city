<?php
	class ModeratorManager {
	
		private $db;
		
		private function mapDBModeratorToModerator($DBModerator) {
		
			$moderator = new Moderator();
			
			$moderator->id = $DBModerator->id; 
			$moderator->login = $DBModerator->login; 
			$moderator->email = $DBModerator->email;
			$moderator->passwordHash = $DBModerator->password_hash; 
			$moderator->passwordResetHash = $DBModerator->password_reset_hash; 
			$moderator->passwordResetTimestamp = $DBModerator->password_reset_timestamp; 
			$moderator->rememberMeToken = $DBModerator->rememberme_token; 
			$moderator->failedLogins = $DBModerator->failed_logins;
			$moderator->lastFailedLogin = $DBModerator->last_failed_login; 
			
			return $moderator;
		}
		
		private function fillDBModeratorWithData($DBModerator, $modelModerator) {
		
			$DBModerator->login = $modelModerator->login;
			$DBModerator->email = $modelModerator->email;
			$DBModerator->password_hash = $modelModerator->passwordHash; 
			$DBModerator->password_reset_hash = $modelModerator->passwordResetHash; 
			$DBModerator->password_reset_timestamp = $modelModerator->passwordResetTimestamp; 
			$DBModerator->rememberme_token = $modelModerator->rememberMeToken; 
			$DBModerator->failed_logins = $modelModerator->failedLogins;
			$DBModerator->last_failed_login = $modelModerator->lastFailedLogin; 
			
			return $DBModerator;
		}
		
		
		public function __construct($connection)
		{
			$this->db = $connection;
		}
		
		
		public function add($newModerator) {
		
			$moderator = new DB\SQL\Mapper($this->db,'mods');
			
			$moderator->login = $newModerator->login;
			$moderator->email = $newModerator->email;
			$moderator->password_hash = $newModerator->passwordHash;
			$moderator->password_reset_hash = $newModerator->passwordResetHash;
			$moderator->password_reset_timestamp = $newModerator->passwordResetTimestamp;
			$moderator->rememberme_token = $newModerator->rememberMeToken;
			$moderator->failed_logins = $newModerator->failedLogins;
			$moderator->last_failed_login = $newModerator->lastFailedLogin;
			
			$moderator->save();
			return $moderator->id;
		}
		
		public function findByID($id) {
		
			$retrievedModerator = new DB\SQL\Mapper($this->db, 'mods');
			$retrievedModerator->load(array('id=?', $id));
			
			if(is_null($retrievedModerator->id))
				return null;
			
			return $this->mapDBModeratorToModerator($retrievedModerator);
		}
		
		public function findByLogin($login) {
			
			$retrievedModerator = new DB\SQL\Mapper($this->db, 'mods');
			$retrievedModerator->load(array('login=?', $login));
			
			if(is_null($retrievedModerator->id))
				return null;
			
			return $this->mapDBModeratorToModerator($retrievedModerator);
		}
		
		public function findByEmail($email) {
			
			$retrievedModerator = new DB\SQL\Mapper($this->db, 'mods');
			$retrievedModerator->load(array('email=?', $email));
			
			if(is_null($retrievedModerator->id))
				return null;
			
			return $this->mapDBModeratorToModerator($retrievedModerator);
		}
		
		public function getAll() {
		
			$retrievedModerator = new DB\SQL\Mapper($this->db, 'mods');
			$retrievedModerator->load();
			
			if(is_null($retrievedModerator->id))
				return null;
			
			$moderators = array();
			
			do { 
				array_push($moderators, $this->mapDBModeratorToModerator($retrievedModerator)); 
				
			} while($retrievedModerator->next());
			
			return $moderators;
		}
		
		public function deleteByID($id) {
		
			$moderatorToDelete = new DB\SQL\Mapper($this->db, 'mods');
			$moderatorToDelete->load(array('id=?', $id));
			
			$moderatorToDelete->erase();
		}
		
		public function deleteByLogin($login) {
		
			$moderatorToDelete = new DB\SQL\Mapper($this->db, 'mods');
			$moderatorToDelete->load(array('login=?', $login));
			
			$moderatorToDelete->erase();
		}
	
		public function deleteAll() {
		
			$this->db->exec('DELETE FROM mods');
		}
		
		public function update($moderator) {
		
			$moderatorToUpdate = new DB\SQL\Mapper($this->db, 'mods');
			$moderatorToUpdate->load(array('id=?', $moderator->id));
			$moderatorToUpdate = $this->fillDBModeratorWithData($moderatorToUpdate, $moderator);
			
			$moderatorToUpdate->save();
		}
	}
?>
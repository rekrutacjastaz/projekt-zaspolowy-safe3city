<?php
	require_once ($_SERVER['DOCUMENT_ROOT'].'/config/config.php'); 
	require_once ($_SERVER['DOCUMENT_ROOT'].'/libraries/f3/base.php'); 
	
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/managers/UserManager.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/managers/ModeratorManager.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/managers/AdminManager.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/managers/AddressManager.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/managers/IncidentManager.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/managers/IncidentTypeManager.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/classes/database/managers/LoginManager.php');


	class DAO {

		private $db;
		
		public $user;
		public $moderator;
		public $admin;
		public $address;
		public $incident;
		public $incidentType;
		
		public $login;
		
		public function __construct()
		{
			$this->db = new DB\SQL(
				'mysql:host='. DB_HOST .';dbname='. DB_NAME . ';charset=utf8',
				DB_USER, DB_PASS
			);
			
			$this->user = new UserManager($this->db);
			$this->moderator = new ModeratorManager($this->db);
			$this->admin = new AdminManager($this->db);
			$this->address = new AddressManager($this->db);
			$this->incidentType = new IncidentTypeManager($this->db);
			
			$this->incident = new IncidentManager($this->db, $this->address, $this->incidentType);
			
			$this->login = new LoginManager($this->user, $this->moderator, $this->admin);
		}
	}
?>
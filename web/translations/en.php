<?php


define("MESSAGE_ACCOUNT_NOT_ACTIVATED", "Your account is not activated yet.");
define("MESSAGE_CAPTCHA_WRONG", "Captcha was wrong!");
define("MESSAGE_COOKIE_INVALID", "Invalid cookie");
define("MESSAGE_DATABASE_ERROR", "Database connection problem.");
define("MESSAGE_EMAIL_ALREADY_EXISTS", "Email  is already registered.");
define("MESSAGE_EMAIL_CHANGE_FAILED", "Your email changing failed.");
define("MESSAGE_EMAIL_CHANGED_SUCCESSFULLY", "Email  has been changed successfully.");
define("MESSAGE_EMAIL_EMPTY", "Email cannot be empty");
define("MESSAGE_EMAIL_INVALID", "Email is not in a valid email format");
define("MESSAGE_EMAIL_SAME_LIKE_OLD_ONE", "Email  is the same as your current one.");
define("MESSAGE_EMAIL_TOO_LONG", "Email cannot be longer than 64 characters");
define("MESSAGE_LINK_PARAMETER_EMPTY", "Empty link parameter data.");
define("MESSAGE_LOGGED_OUT", "You have been logged out.");

define("MESSAGE_LOGIN_FAILED", "Login failed.");
define("MESSAGE_OLD_PASSWORD_WRONG", "Your OLD password was wrong.");
define("MESSAGE_PASSWORD_BAD_CONFIRM", "Passwords are not the same");
define("MESSAGE_PASSWORD_CHANGE_FAILED", "Password changing failed.");
define("MESSAGE_PASSWORD_CHANGED_SUCCESSFULLY", "Password successfully changed!");
define("MESSAGE_PASSWORD_EMPTY", "Password field was empty");
define("MESSAGE_PASSWORD_RESET_MAIL_FAILED", "Password reset mail NOT successfully sent! ");
define("MESSAGE_PASSWORD_RESET_MAIL_SUCCESSFULLY_SENT", "Password reset mail successfully sent!");
define("MESSAGE_PASSWORD_TOO_SHORT", "Password has a minimum length of 6 characters");
define("MESSAGE_PASSWORD_WRONG", "Wrong password. Try again.");
define("MESSAGE_PASSWORD_WRONG_3_TIMES", "You have entered an incorrect password 3 or more times already. Please wait 30 seconds!");
define("MESSAGE_REGISTRATION_ACTIVATION_NOT_SUCCESSFUL", "No such id/verification code combination here...");
define("MESSAGE_REGISTRATION_ACTIVATION_SUCCESSFUL", "Activation was successful!");
define("MESSAGE_REGISTRATION_FAILED", "Registration failed.");
define("MESSAGE_RESET_LINK_HAS_EXPIRED", "Your reset link has expired.");
define("MESSAGE_VERIFICATION_MAIL_ERROR", "Not send you an verification mail.");
define("MESSAGE_VERIFICATION_MAIL_NOT_SENT", "Verification Mail NOT successfully sent!");
define("MESSAGE_VERIFICATION_MAIL_SENT", "Account has been created successfully and receive email message. ");
define("MESSAGE_USER_DOES_NOT_EXIST", "User does not exist");
define("MESSAGE_USERNAME_BAD_LENGTH", "Username must be between 6-64 characters");
define("MESSAGE_USERNAME_CHANGE_FAILED", "Chosen username renaming failed");
define("MESSAGE_USERNAME_CHANGED_SUCCESSFULLY", "Username has been changed successfully.");
define("MESSAGE_USERNAME_EMPTY", "Username field was empty");
define("MESSAGE_USERNAME_EXISTS", "Username is already taken.");
define("MESSAGE_USERNAME_INVALID", "Username does not fit the name scheme.");
define("MESSAGE_USERNAME_SAME_LIKE_OLD_ONE", "Username is the same as your current one.");


define("WORDING_BACK_TO_LOGIN", "Back to Login Page");
define("WORDING_CHANGE_EMAIL", "Change email");
define("WORDING_CHANGE_PASSWORD", "Change password");
define("WORDING_CHANGE_USERNAME", "Change username");
define("WORDING_CURRENTLY", "currently");
define("WORDING_EDIT_USER_DATA", "Edit user data");
define("WORDING_EDIT_YOUR_CREDENTIALS", "You are logged in and can edit data");
define("WORDING_FORGOT_MY_PASSWORD", "I forgot my password");
define("WORDING_LOGIN", "Log in");
define("WORDING_LOGOUT", "Log out");
define("WORDING_NEW_EMAIL", "New email");
define("WORDING_NEW_PASSWORD", "New password");
define("WORDING_NEW_PASSWORD_REPEAT", "Repeat new password");
define("WORDING_NEW_USERNAME", "New username");
define("WORDING_OLD_PASSWORD", "OLD Password");
define("WORDING_PASSWORD", "Password");
define("WORDING_REGISTER", "Register");
define("WORDING_REGISTER_NEW_ACCOUNT", "Register new account");
define("WORDING_REGISTRATION_CAPTCHA", "Please enter these characters");
define("WORDING_REGISTRATION_EMAIL", "Email");
define("WORDING_REGISTRATION_PASSWORD", "Password");
define("WORDING_REGISTRATION_PASSWORD_REPEAT", "Password repeat");
define("WORDING_REGISTRATION_USERNAME", "Username");
define("WORDING_REMEMBER_ME", "Keep me logged in");
define("WORDING_REQUEST_PASSWORD_RESET", "Request a password reset giving username.");
define("WORDING_RESET_PASSWORD", "Reset my password");
define("WORDING_SUBMIT_NEW_PASSWORD", "Submit new password");
define("WORDING_USERNAME", "Username");
define("WORDING_YOU_ARE_LOGGED_IN_AS", "You are logged in as ");


<?php


define("MESSAGE_ACCOUNT_NOT_ACTIVATED", "Konto nie zostało jeszcze aktywowane.");
define("MESSAGE_CAPTCHA_WRONG", "Błędny kod z obrazka");
define("MESSAGE_COOKIE_INVALID", "Nieprawidłowy plik cookie");
define("MESSAGE_DATABASE_ERROR", "Błąd połączenia z bazą danych.");
define("MESSAGE_EMAIL_ALREADY_EXISTS", "Email jest już zarejestrowany.");
define("MESSAGE_EMAIL_CHANGE_FAILED", "Zmiana e-mail nie powiodła się.");
define("MESSAGE_EMAIL_CHANGED_SUCCESSFULLY", "E-mail został pomyślnie zmieniony .");
define("MESSAGE_EMAIL_EMPTY", "E-mail nie może być pusty");
define("MESSAGE_EMAIL_INVALID", "E-mail nie jest napisany w poprawnym formacie");
define("MESSAGE_EMAIL_SAME_LIKE_OLD_ONE", "E-mail jest taki sam jak obecny.");
define("MESSAGE_EMAIL_TOO_LONG", "E-mail nie może być dłuższy niż 64 znaki");
define("MESSAGE_LINK_PARAMETER_EMPTY", "Puste dane parametrów łącza");
define("MESSAGE_LOGGED_OUT", "Zostałeś wylogowany.");

define("MESSAGE_LOGIN_FAILED", "Logowanie nie powiodło się");
define("MESSAGE_OLD_PASSWORD_WRONG", "Stare hasło jest błędne.");
define("MESSAGE_PASSWORD_BAD_CONFIRM", "Hasła nie są takie same");
define("MESSAGE_PASSWORD_CHANGE_FAILED", "Zmiana hasła nie powiodła się");
define("MESSAGE_PASSWORD_CHANGED_SUCCESSFULLY", "Hasło zmienione pomyślnie!");
define("MESSAGE_PASSWORD_EMPTY", "Hasło jest puste");
define("MESSAGE_PASSWORD_RESET_MAIL_FAILED", "E-mail resetujący hasło NIE został wysłany poprawnie!");
define("MESSAGE_PASSWORD_RESET_MAIL_SUCCESSFULLY_SENT", "E-mail resetujący hasło został wysłany poprawnie!");
define("MESSAGE_PASSWORD_TOO_SHORT", "Hasło ma długość co najmniej 6 znaków");
define("MESSAGE_PASSWORD_WRONG", "Błędne hasło. Spróbuj ponownie.");
define("MESSAGE_PASSWORD_WRONG_3_TIMES", "Wpisałeś błędne hasło 3 lub więcej razy. Poczekaj 30 sekund!");
define("MESSAGE_REGISTRATION_ACTIVATION_NOT_SUCCESSFUL", "Nie istnieje taka para identyfikator - kod weryfikacyjny...");
define("MESSAGE_REGISTRATION_ACTIVATION_SUCCESSFUL", "Aktywacja przebiegła poprawnie!");
define("MESSAGE_REGISTRATION_FAILED", "Rejestracja nie powiodła się.");
define("MESSAGE_RESET_LINK_HAS_EXPIRED", "Link resetowania wygasł.");
define("MESSAGE_VERIFICATION_MAIL_ERROR", "Nie udało się wysłać maila aktywacyjnego.");
define("MESSAGE_VERIFICATION_MAIL_NOT_SENT", "E-mial aktywacyjny nie został wysłany poprawnie!");
define("MESSAGE_VERIFICATION_MAIL_SENT", "Konto zostało utworzone i otrzymałeś wiadomość e-mail. ");
define("MESSAGE_USER_DOES_NOT_EXIST", "Użytkownik nie istnieje");
define("MESSAGE_USERNAME_BAD_LENGTH", "Nazwa użytkownika musi być z przedziału 6-64 znaki");
define("MESSAGE_USERNAME_CHANGE_FAILED", "Zmiana nazwy użytkownika nie powiodła się.");
define("MESSAGE_USERNAME_CHANGED_SUCCESSFULLY", "Nazwa użytkownika zmieniona poprawnie.");
define("MESSAGE_USERNAME_EMPTY", "Nazwa użytkownika jest pusta");
define("MESSAGE_USERNAME_EXISTS", "Nazwa użytkownika jest już zajęta. ");
define("MESSAGE_USERNAME_INVALID", "Nazwa użytkownika nie pasuje do schematu nazw.");
define("MESSAGE_USERNAME_SAME_LIKE_OLD_ONE", "Nazwa użytkownika jest taka sama jak obecna. ");


define("WORDING_BACK_TO_LOGIN", "Wróć do strony logowania");
define("WORDING_CHANGE_EMAIL", "Zmień e-mail");
define("WORDING_CHANGE_PASSWORD", "Zmień hasło");
define("WORDING_CHANGE_USERNAME", "Zmień nazwę użytkownika");
define("WORDING_CURRENTLY", "obecnie");
define("WORDING_EDIT_USER_DATA", "Edytuj dane użytkownika");
define("WORDING_EDIT_YOUR_CREDENTIALS", "Jesteś zalogowany i możesz edytować dane");
define("WORDING_FORGOT_MY_PASSWORD", "Zapomniałem hasła");
define("WORDING_LOGIN", "Zaloguj");
define("WORDING_LOGOUT", "Wyloguj");
define("WORDING_NEW_EMAIL", "Nowy e-mail");
define("WORDING_NEW_PASSWORD", "Nowe hasło");
define("WORDING_NEW_PASSWORD_REPEAT", "Powtórz nowe hasło");
define("WORDING_NEW_USERNAME", "Nowa nazwa użytkownika");
define("WORDING_OLD_PASSWORD", "STARE hasło");
define("WORDING_PASSWORD", "Hasło");
define("WORDING_REGISTER", "Rejestracja");
define("WORDING_REGISTER_NEW_ACCOUNT", "Zarejestruj nowe konto");
define("WORDING_REGISTRATION_CAPTCHA", "Przepisz te znaki");
define("WORDING_REGISTRATION_EMAIL", "E-mail");
define("WORDING_REGISTRATION_PASSWORD", "Hasło");
define("WORDING_REGISTRATION_PASSWORD_REPEAT", "Powtórz hasło");
define("WORDING_REGISTRATION_USERNAME", "Nazwa użytkownika");
define("WORDING_REMEMBER_ME", "Zapamiętaj mnie");
define("WORDING_REQUEST_PASSWORD_RESET", "Poproś o zresetowanie hasła podając nazwę użytkownika");
define("WORDING_RESET_PASSWORD", "Zresetuj hasło");
define("WORDING_SUBMIT_NEW_PASSWORD", "Zatwierdź hasło");
define("WORDING_USERNAME", "Nazwa użytkownika");
define("WORDING_YOU_ARE_LOGGED_IN_AS", "Jesteś zalogowany jako ");


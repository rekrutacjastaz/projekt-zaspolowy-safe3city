<?php

if (isset($login)) {

    if ($login->errors) {
		
		echo '<div id="alertBox" class="error">';
		
        foreach ($login->errors as $error) {
            echo $error;
        }
		
		echo '</div>';
    }
    if ($login->messages) {
		
		echo '<div id="alertBox" class="ok">';
		
        foreach ($login->messages as $message) {
            echo $message;
        }
		
		echo '</div>';
    }
} 
?>

<?php

if (isset($registration)) {
    if ($registration->errors) {
		
		echo '<div id="alertBox" class="error">';
		
        foreach ($registration->errors as $error) {
            echo $error;
        }
		
		echo '</div>';
    }
    if ($registration->messages) {
		
		echo '<div id="alertBox" class="ok">';
		
        foreach ($registration->messages as $message) {
            echo $message;
        }
		
		echo '</div>';
    }
} 
?>
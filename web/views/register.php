<?php include('_header.php'); ?>


<?php if (!$registration->registration_successful && !$registration->verification_successful) { ?>
<form method="post" action="register.php" name="registerform" enctype="multipart/form-data">

	<table class="register">
		<tr>
			<td>
				<label for="user_name"><?php echo WORDING_REGISTRATION_USERNAME; ?></label>
			</td>
			<td>
				<input id="user_name" type="text" pattern="[a-zA-Z0-9]{6,64}" name="user_name" required />
			</td>
		</tr>
		<tr>
			<td>
				<label for="user_email"><?php echo WORDING_REGISTRATION_EMAIL; ?></label>
			</td>
			<td>
				<input id="user_email" type="email" name="user_email" required />
			</td>
		</tr>
		<tr>
			<td>
				<label for="user_password_new"><?php echo WORDING_REGISTRATION_PASSWORD; ?></label>
			</td>
			<td>
				<input id="user_password_new" type="password" name="user_password_new" pattern=".{6,}" required autocomplete="off" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="user_password_repeat"><?php echo WORDING_REGISTRATION_PASSWORD_REPEAT; ?></label>
			</td>
			<td>
				<input id="user_password_repeat" type="password" name="user_password_repeat" pattern=".{6,}" required autocomplete="off" />
			</td>
		</tr>

		<tr>
			<td></td>
			<td>
				<img src="tools/showCaptcha.php" alt="captcha" />
			</td>
		</tr>
		<tr>
			<td>
				<label><?php echo WORDING_REGISTRATION_CAPTCHA; ?></label>
			</td>
			<td>
				<input type="text" name="captcha" required />
			</td>
		</tr>
	</table>
	<input class="form-button" type="submit" name="register" value="<?php echo WORDING_REGISTER; ?>"/>
	
</form>
<?php } ?>

<?php include('_footer.php'); ?>

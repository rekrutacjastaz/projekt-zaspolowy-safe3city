<div id="menu"> 
		<?php
		
			if(isset($_GET['link'])) {
				$_SESSION['link'] = $_GET['link'];
			}
			else $_SESSION['link'] = "1";
			
			if(isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1) {
				$menu_itmes = array(
					"1" => array("index.php" => 'Strona główna'),
					"2" => array("edit.php" => 'Edycja użytkownika'),
					"3" => array("logout.php" => "Wyloguj")
				);
			}
			else {
				$menu_itmes = array(
					"1" => array("index.php" => 'Strona główna'),
					"2" => array("register.php" => 'Rejestracja'),
					"3" => array("login.php" => 'Logowanie')
				);
			}
			
			if (array_values($menu_itmes[$_SESSION['link']])[0] == "Wyloguj")
				$_SESSION['link'] = "1";
			
			while (list($key, $value) = each($menu_itmes)) {
				while (list($key2, $value2) = each($value)) {		
					if ($key == $_SESSION['link'])
						echo "<a href='$key2?link=$key' class='clicked_menu_link'>$value2</a>";
					else
						echo "<a href='$key2?link=$key' class='menu_link'>$value2</a>";
				}	
			}
		?>
</div>
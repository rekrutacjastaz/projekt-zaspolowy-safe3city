<?php include('_header.php'); ?>

<form method="post" action="edit.php" name="edit_form_name">
	<table class="edit">
		<tr>
			<td>
				<label for="name"><?php echo WORDING_NEW_USERNAME; ?></label>
			</td>
			<td>
				<input id="name" type="text" name="name" pattern="[a-zA-Z0-9]{2,64}" required
					placeholder="<?php echo WORDING_CURRENTLY; ?>: <?php echo $_SESSION['name']; ?>" />
			</td>
		</tr>
	</table>
	<input class="form-button" type="submit" name="edit_submit_name" 
		value="<?php echo WORDING_CHANGE_USERNAME; ?>" />
</form>


<form method="post" action="edit.php" name="edit_form_email">
	<table class="edit">
		<tr>
			<td>
				<label for="email"><?php echo WORDING_NEW_EMAIL; ?></label>
			</td>
			<td>
				<input id="email" type="email" name="email" required 
					placeholder=" <?php echo WORDING_CURRENTLY; ?>: <?php echo $_SESSION['email']; ?>"/>
			</td>
		</tr>
	</table>
    <input class="form-button" type="submit" name="edit_submit_email" value="<?php echo WORDING_CHANGE_EMAIL; ?>" />
</form>


<form method="post" action="edit.php" name="edit_form_password">
	<table class="edit">
		<tr>
			<td>
				<label for="password_old"><?php echo WORDING_OLD_PASSWORD; ?></label>
			</td>
			<td>
				<input id="password_old" type="password" name="password_old" autocomplete="off" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="password_new"><?php echo WORDING_NEW_PASSWORD; ?></label>
			</td>
			<td>
				<input id="password_new" type="password" name="password_new" autocomplete="off" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="password_repeat"><?php echo WORDING_NEW_PASSWORD_REPEAT; ?></label>
			</td>
			<td>
				<input id="password_repeat" type="password" name="password_repeat" autocomplete="off" />
			</td>
		</tr>
	</table>
    <input class="form-button" type="submit" name="edit_submit_password" value="<?php echo WORDING_CHANGE_PASSWORD; ?>" />
</form>

<?php include('_footer.php'); ?>
